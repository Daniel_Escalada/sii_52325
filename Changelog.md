# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.2] - 2021-11-02
### Added
- The ball size reduces over time

## [1.1.1] - 2021-11-02
### Added
- Added movement to players
- Added movement to ball

## [1.1.0] - 2021-11-01
### Added
- Identification added to de the .h and .cpp files


