# Pong
Juego de tenis para dos jugadores

## Intrucciones
- Cada jugador controla una raqueta que mueve verticalmente para intentar devolver la pelota a jugador contrario
- Si la pelota no es devuelta, es decir, sobrepasa la raqueta de un jugador, el jugador contrario recibe un punto
- La pelota se va haciendo más pequeña con el paso del tiempo

## Controles

### Jugador 1
 Arriba -> 'w'
 Abajo  -> 's'
 
### Jugador 2
 Arriba -> 'o'
 Abajo  -> 'l'
 
